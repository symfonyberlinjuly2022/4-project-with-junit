<?php

declare(strict_types=1);

namespace App\Tests\Integration;

use App\Entity\Book;
use App\Entity\PriceHistory;
use App\Entity\Publisher;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class DoctrineOnFlushListenerTest extends KernelTestCase
{
    public function testPriceHistoryFeature(): void
    {
        $publisher = new Publisher();
        $publisher->setName('Pse? Sepse!');

        $book = new Book();
        $book->setPublisher($publisher);
        $book->setTitle('Foo Bar? Buzz!');
        $book->setIsbn('123-456-789');
        $book->setPrice('20');

        self::bootKernel();

        static::$kernel->getContainer()->get('doctrine.orm.entity_manager')->persist($publisher);
        static::$kernel->getContainer()->get('doctrine.orm.entity_manager')->persist($book);
        static::$kernel->getContainer()->get('doctrine.orm.entity_manager')->flush();

        $this->assertNull($book->getOldPrice());

        $book->setPrice('19');

        static::$kernel->getContainer()->get('doctrine.orm.entity_manager')->flush();

        $this->assertSame('19', $book->getPrice());
        $this->assertSame('20', $book->getOldPrice());

        $prices = static::$kernel
            ->getContainer()
            ->get('doctrine')
            ->getRepository(PriceHistory::class)
            ->findByBook($book->getIsbn());

        $this->assertNotEmpty($prices);

        /** @var PriceHistory $historyItem */
        $historyItem = $prices[0];

        $this->assertSame('20', $historyItem->getPrice());
    }
}
