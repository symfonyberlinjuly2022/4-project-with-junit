<?php

declare(strict_types=1);

namespace App\Tests\Functional;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class ListControllerTest extends WebTestCase
{
    public function testListAuthors(): void
    {
        self::createClient()->request(Request::METHOD_GET, '/list/authors');

        self::assertResponseIsSuccessful();
    }

    public function testListBooks(): void
    {
        self::createClient()->request(Request::METHOD_GET, '/list/books');

        self::assertResponseIsSuccessful();
    }

    public function testListPublishers(): void
    {
        self::createClient()->request(Request::METHOD_GET, '/list/publishers');

        self::assertResponseIsSuccessful();
    }
}