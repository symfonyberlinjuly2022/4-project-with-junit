<?php

declare(strict_types=1);

namespace App\Tests\Functional;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class CreateControllerTest extends WebTestCase
{
    public function testCreatePublisherAction(): void
    {
        $client = self::createClient();
        $client->followRedirects();

        $crawler = $client->request(Request::METHOD_GET, '/create/publishers');
        $form = $crawler->selectButton('Submit')->form();

        $form['publisher[name]'] = 'Foo Bar';
        $client->submit($form);

        $this->assertResponseIsSuccessful();
        $this->assertStringContainsString('Foo Bar', $client->getResponse()->getContent());
     }
}