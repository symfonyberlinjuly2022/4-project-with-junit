<?php

declare(strict_types=1);

namespace App\Tests\Functional;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class IndexActionTest extends WebTestCase
{
    public function testIndexAction(): void
    {
        self::createClient()->request(Request::METHOD_GET, '/');

        self::assertResponseIsSuccessful();
    }
}